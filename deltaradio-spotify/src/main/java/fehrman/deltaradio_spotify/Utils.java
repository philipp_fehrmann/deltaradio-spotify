package fehrman.deltaradio_spotify;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.LocalDateTime;
import java.time.ZoneId;

/**
 * Utility Class holding various methods.
 * 
 * @author Philipp
 *
 */
public class Utils {

	/**
	 * Download a website
	 * 
	 * @param url
	 *            The website to be downloaded
	 * @return Returns the content of the website
	 * @throws MalformedURLException
	 *             Is thrown, if a malformed URL is entered
	 */
	public static String downloadWebsite(String url) throws MalformedURLException {
		URL website = new URL(url);
		StringBuilder sb = new StringBuilder();
		try (InputStream in = website.openStream(); BufferedReader br = new BufferedReader(new InputStreamReader(in))) {
			while (br.ready()) {
				while (br.ready()) {
					sb.append(br.readLine() + "\n");
				}
				sleep(200);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return sb.toString();
	}

	/**
	 * Returns the current Date and time as expected by the iris websocket
	 * 
	 * @return
	 */
	public static String getCurrentDate() {
		return LocalDateTime.now(ZoneId.systemDefault()).minusMinutes(15).toString();
	}

	/**
	 * Make sleeping easier
	 * @param ms The duration to sleep in ms.
	 */
	public static void sleep(long ms) {
		try {
			Thread.sleep(ms);
		} catch (InterruptedException ex) {
			ex.printStackTrace();
		}
	}
	
	public static String readFileWithoutNL(String path) {
		File f = new File(path);
		StringBuilder ret = new StringBuilder();
		
		try (BufferedReader br = new BufferedReader(new FileReader(f));){
			while(br.ready()) {
				ret.append(br.readLine());
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return ret.toString();
	}
	
	public static boolean argsContains(String [] args, String argument, boolean caseSensitive) {
		for(String s : args) {
			if(caseSensitive) {
				if(s.equals(argument)) {
					return true;
				}
			} else {
				if(s.equalsIgnoreCase(argument)) {
					return true;
				}
			}
		}
		return true;
	}
	
}
