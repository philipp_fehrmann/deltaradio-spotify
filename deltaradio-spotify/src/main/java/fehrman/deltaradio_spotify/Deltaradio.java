package fehrman.deltaradio_spotify;

import java.net.MalformedURLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

public class Deltaradio {
	private static Song currentSong = null;
	private static Song lastSong = null;

	public static void checkCurrentSong() {

		// Download the json
		String json = null;
		try {
			String time = Utils.getCurrentDate();
			json = Utils.downloadWebsite("http://deltaradio.de/iris-search-live.json?start=" + time);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}

		// Parse JSON Object
		if (json != null) {
			JSONObject obj = null;
			
			try {
				obj = new JSONObject(json).getJSONObject("result");
			} catch (Exception e) {
				System.out.println(json);
				e.printStackTrace();
				return;
			}
			
			JSONArray songs = obj.getJSONArray("entry");

			// Liste mit allen song erstellen
			List<Song> songList = new ArrayList<>();

			Iterator<Object> it = songs.iterator();
			while (it.hasNext()) {
				JSONObject jsonEntry = (JSONObject) it.next();
				LocalDateTime airtime = LocalDateTime.parse(jsonEntry.getString("airtime").replaceAll("\\+.*", ""));
				int duration = jsonEntry.getInt("duration");
				
				JSONObject jsonSong = (JSONObject) jsonEntry.getJSONObject("song").getJSONArray("entry").get(0);
				String title = jsonSong.getString("title");
				String artistName = ((JSONObject) jsonSong.getJSONObject("artist").getJSONArray("entry").get(0))
						.getString("name");
				Song song = new Song(airtime, title, artistName, duration);
				songList.add(song);
			}

			if (songList.isEmpty()) {
				return;
			}

			// Sort the list
			Collections.sort(songList);

			// Assign to variable
			lastSong = currentSong;
			currentSong = songList.get(0);
		}
	}

	public static Song getCurrentSong() {
		return currentSong;
	}

	public static boolean isNewSong() {
		return !currentSong.equals(lastSong);
	}

	public static void main(String[] args) {
		int sleeptime = 5000;
		
		// which input should be used for the token?
		if(Utils.argsContains(args, "-console", false)) {
			
			// input via console. To be used, if the application is running on a server
			Spotify.tokenInput = new ConsoleInput();
		} else {
			
			// login via the webbrowser
			Spotify.tokenInput = new WebLogIn();
		}
		while (true) {
			Deltaradio.checkCurrentSong();
			if (Deltaradio.isNewSong()) {
				System.out.println("Neuer Song: " + Deltaradio.getCurrentSong());
				Spotify.addSong(Deltaradio.currentSong);
				sleeptime = Deltaradio.getCurrentSong().getDuration()*1000;
			}
			Utils.sleep(sleeptime);
			sleeptime = 5000;
		}
	}
}
