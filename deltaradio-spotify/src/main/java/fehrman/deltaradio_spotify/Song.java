package fehrman.deltaradio_spotify;

import java.time.LocalDateTime;

public class Song implements Comparable<Song>{
	private LocalDateTime airtime;
	private String title;
	private String artist;
	private int duration;
	
	public Song(LocalDateTime airtime, String title, String artist, int duration) {
		this.airtime = airtime;
		this.title = title;
		this.artist = artist;
		this.duration = duration;
	}
	
	public LocalDateTime getAirtime() {
		return airtime;
	}
	
	public String getTitle() {
		return title;
	}
	
	public String getArtist() {
		return artist;
	}
	
	public int getDuration() {
		return duration;
	}

	@Override
	public boolean equals(Object o) {
		if(o == null) {
			return false;
		}
		
		if(o.getClass() != this.getClass()) {
			return false;
		}
		
		Song s = (Song) o;
		
		return this.toString().equals(s.toString());
	}
	
	@Override
	public int compareTo(Song o) {
		return airtime.compareTo(o.airtime) * -1;
	}
	
	@Override
	public String toString() {
		return String.format("%15s, %s mit '%s'", airtime, artist, title);
	}
}
