package fehrman.deltaradio_spotify;

import java.util.Arrays;
import java.util.List;

import com.wrapper.spotify.Api;

public class WebLogIn implements TokenInput {
	private boolean loggedIn = false;
	private String code = "";
	@Override
	public String getInitialToken() {
		if (!"".equals(code)) {
			login();
		}
		return null;
	}

	private void login() {

		if (loggedIn) {
			// Just return and assume, that the tokens are still valid.
			return;
		}
		
		Spotify.api = Api.builder().clientId(Spotify.clientId).clientSecret(Spotify.clientSecret).redirectURI(Spotify.redirectURI).build();

		/*
		 * Set the necessary scopes that the application will need from the user
		 */
		final List<String> scopes = Arrays.asList("playlist-modify-public");

		/* Set a state. This is used to prevent cross site request forgeries. */
		final String state = "someExpectedStateString";

		String authorizeURL = Spotify.api.createAuthorizeURL(scopes, state);

		Object semaphore = new Object();

		new Thread(new Runnable() {

			@Override
			public void run() {
				WebBrowser.semaphore = semaphore;
				WebBrowser.website = authorizeURL;
				WebBrowser.start();
			}
		}).start();

		// Wait for the user to be logged in
		synchronized (semaphore) {
			try {
				semaphore.wait();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		// close the webbrowser.
		WebBrowser.exit();
		code = WebBrowser.code;
	}
	
}
