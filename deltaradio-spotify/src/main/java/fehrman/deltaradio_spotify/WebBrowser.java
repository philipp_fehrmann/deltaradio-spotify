package fehrman.deltaradio_spotify;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.stage.Stage;
import javafx.scene.*;
import javafx.scene.web.*;
import javafx.scene.layout.*;

public class WebBrowser extends Application{
	public static String website;
	public static String code = "";
	public static Object semaphore;
	public static Stage stage;
	
	public static void start() {
		Application.launch();
	}

	@Override
	public void start(Stage stage) throws Exception {
		WebView myWebView = new WebView();
		WebEngine engine = myWebView.getEngine();
		engine.load(website);
		
		new Waiter(engine).start();
		
		VBox root = new VBox();
		root.getChildren().addAll(myWebView);
		
		Scene scene = new Scene(root, 500, 800);
		stage.setScene(scene);
		
		stage.show();
	}
	
	@Override
	public void stop() {
		if("".equals(code)) {
			System.out.println("Kein Code gefunden. Anwendung wird beendet.");
			System.exit(0);
		}
	}
	
	public static void exit() {
		Platform.exit();
	}
	
	class Waiter extends Thread {
		WebEngine engine;
		
		public Waiter(WebEngine engine) {
			this.engine = engine;
		}
		
		@Override
		public void run() {
			
			// wait until the user has logged in
			while(!engine.getLocation().startsWith(Spotify.redirectURI)) {
				try {
					sleep(10);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			
			// Extract the get parameter
			String regex = ".*\\?.*code=(.*?)(&|$)";
			Matcher m = Pattern.compile(regex).matcher(engine.getLocation());
			
			if(m.find()) {
				WebBrowser.code = m.group(1);
			} else {
				System.out.println("Code not found. URL was " + engine.getLocation());
			}
			
			synchronized (semaphore) {
				semaphore.notifyAll();
			}
		}
	}
}
