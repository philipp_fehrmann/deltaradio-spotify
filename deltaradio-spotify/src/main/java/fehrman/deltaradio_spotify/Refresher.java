package fehrman.deltaradio_spotify;

import java.io.IOException;

import com.wrapper.spotify.exceptions.WebApiException;
import com.wrapper.spotify.models.RefreshAccessTokenCredentials;

public class Refresher extends Thread {
	@Override
	public void run() {
		while (true) {
			RefreshAccessTokenCredentials refreshCredentials = null;

			// get new tokens
			try {
				refreshCredentials = Spotify.api.refreshAccessToken().build().get();
				System.out.println("New token: " + refreshCredentials.getAccessToken());
				//Spotify.api.setRefreshToken(refreshCredentials.getAccessToken());
			} catch (IOException | WebApiException e) {
				e.printStackTrace();
			}

			// wait for token to expire
			int sleeptime = (refreshCredentials.getExpiresIn() - 60) * 1000;
			if(sleeptime < 0) {
				sleeptime = 1000;
			}
			Utils.sleep(sleeptime);
		}
	}
}