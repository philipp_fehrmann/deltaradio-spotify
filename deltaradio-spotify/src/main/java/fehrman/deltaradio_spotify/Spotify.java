package fehrman.deltaradio_spotify;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.wrapper.spotify.Api;
import com.wrapper.spotify.exceptions.WebApiException;
import com.wrapper.spotify.methods.AddTrackToPlaylistRequest;
import com.wrapper.spotify.methods.PlaylistCreationRequest;
import com.wrapper.spotify.methods.TrackSearchRequest;
import com.wrapper.spotify.methods.UserPlaylistsRequest;
import com.wrapper.spotify.models.AuthorizationCodeCredentials;
import com.wrapper.spotify.models.Page;
import com.wrapper.spotify.models.Playlist;
import com.wrapper.spotify.models.RefreshAccessTokenCredentials;
import com.wrapper.spotify.models.SimplePlaylist;
import com.wrapper.spotify.models.Track;
import com.wrapper.spotify.models.User;

public class Spotify {
	public static int semaphore;
	public static Api api;
	private static User user = null;
	private static String playlistName = "delta Radio";
	private static boolean loggedIn = false;
	private static Playlist playlist;
	public static TokenInput tokenInput;

	static final String clientId = Utils.readFileWithoutNL("clientid");
	static final String clientSecret = Utils.readFileWithoutNL("clientsecret");
	public static final String redirectURI = "http://www.dillipp.de/GenreSorter/";

	private static Track getTrack(Song song) {
		String query = "track:" + song.getTitle() + " artist:" + song.getArtist();
		TrackSearchRequest request = api.searchTracks(query).query(query).build();
		Track track = null;

		// Retrieve a song
		try {
			Page<Track> page = request.get();
			List<Track> tracks = page.getItems();

			// Lets assume that the first hit is the best.
			if (!tracks.isEmpty()) {
				track = tracks.get(0);
			}
		} catch (Exception e) {
			System.out.println("Could not find songs.");
			e.printStackTrace();
		}

		return track;
	}

	public static void addSong(Song s) {
		getInitialAuthorization();
		String playlist = createPlaylist(playlistName);
		addSongToPlayList(s, playlist);
		// TODO: Remove a song, if more than 300 are in the list
	}
	
	public static void getInitialAuthorization() {
		String code = tokenInput.getInitialToken();
		
		AuthorizationCodeCredentials authorizationCodeCredentials = null;
		try {
			authorizationCodeCredentials = api.authorizationCodeGrant(code).build().get();
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (authorizationCodeCredentials != null) {
			api.setAccessToken(authorizationCodeCredentials.getAccessToken());
			api.setRefreshToken(authorizationCodeCredentials.getRefreshToken());
		} else {
			System.out.println("Could not get an access token.");
			return;
		}

		try {
			user = api.getMe().build().get();
		} catch (IOException | WebApiException e) {
			e.printStackTrace();
		}

		System.out.println("Login successfull");
		loggedIn = true;
		
		new Refresher().start();
	}

	private static String createPlaylist(String name) {

		// First check, if a playlist exists
		final UserPlaylistsRequest getPlaylistsRequest = api.getPlaylistsForUser(getUser().getId()).build();

		try {
			Page<SimplePlaylist> playlistsPage = getPlaylistsRequest.get();

			for (SimplePlaylist playlist : playlistsPage.getItems()) {
				if (playlist.getName().equals(name)) {
					return playlist.getId();
				}
			}
		} catch (Exception e) {
			System.out.println("Something went wrong while retrieving the playlists! " + e.getMessage());
			e.printStackTrace();
		}

		// Create a new playlist
		final PlaylistCreationRequest createPlaylistRequest = api.createPlaylist(getUser().getId(), name).publicAccess(true)
				.build();

		try {
			final Playlist playlist = createPlaylistRequest.get();

			System.out.println("You just created this playlist!");
			System.out.println("Its title is " + playlist.getName());
			return playlist.getId();
		} catch (Exception e) {
			System.out.println("Something went wrong while creating a new playlist! " + e.getMessage());
			e.printStackTrace();
		}
		return null;
	}

	private static void addSongToPlayList(Song s, String playlistID) {

		if (s == null) {
			System.out.println("Error while adding song to playlist. Song is null.");
			return;
		}
		// Get track and add it to a a playlist
		List<String> tracksToAdd = new ArrayList<>();
		Track t = getTrack(s);
		if (t == null) {
			return;
		}
		tracksToAdd.add(t.getUri());

		// Index starts at 0
		final int insertIndex = 0;

		final AddTrackToPlaylistRequest request = api.addTracksToPlaylist(getUser().getId(), playlistID, tracksToAdd)
				.position(insertIndex).build();

		try {
			request.get();
		} catch (Exception e) {
			System.out.println("Something went wrong while adding song to the playlist!" + e.getMessage());
			e.printStackTrace();
		}
	}

	private static void fitPlaylistSize(String playlistID, int size) {

	}

	public static User getUser() {
		return user;
	}

	public static void setUser(User user) {
		Spotify.user = user;
	}
}
