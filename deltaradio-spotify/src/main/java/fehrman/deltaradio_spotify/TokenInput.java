package fehrman.deltaradio_spotify;

public interface TokenInput {
	public String getInitialToken();
}
