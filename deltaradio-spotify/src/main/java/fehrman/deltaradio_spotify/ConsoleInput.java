package fehrman.deltaradio_spotify;

import java.util.Scanner;

public class ConsoleInput implements TokenInput {
	private String token;
	
	public ConsoleInput() {
		token = null;
	}
	
	@Override
	public String getInitialToken() {
		if(token == null) {
			getToken();
		}
		
		return token;
	}
	
	private void getToken() {
		Scanner sc = new Scanner(System.in);
		token = sc.nextLine();
		sc.close();
	}

}
